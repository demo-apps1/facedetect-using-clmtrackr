let outputWidth;
let outputHeight;

let faceTracker; // Face Tracking
let videoInput;

let imgSpidermanMask;

let selected = -1; // Default no filter

/*
 * **p5.js** library automatically executes the `preload()` function. Basically, it is used to load external files. In our case, we'll use it to load the images for our filters and assign them to separate variables for later use.
*/
function preload()
{
    // Spiderman Mask Filter asset
    imgSpidermanMask = loadImage("frame-transparent-1.png");
}

/**
 * In p5.js, `setup()` function is executed at the beginning of our program, but after the `preload()` function.
 */
function setup()
{
    const maxWidth = Math.min(windowWidth, windowHeight);
    pixelDensity(1);
    outputWidth = maxWidth;
    outputHeight = maxWidth * 0.75; // 4:3

    createCanvas(outputWidth, outputHeight);

    // webcam capture
    videoInput = createCapture(VIDEO);
    videoInput.size(outputWidth, outputHeight);
    videoInput.hide();

    // select filter
    const sel = createSelect();
    const selectList = ['Eye glass']; // list of filters
    sel.option('Select Filter', -1); // Default no filter
    for (let i = 0; i < selectList.length; i++)
    {
        sel.option(selectList[i], i);
    }
    sel.changed(applyFilter);

    // tracker
    faceTracker = new clm.tracker();
    faceTracker.init();
    faceTracker.start(videoInput.elt);
}

// callback function
function applyFilter()
{
    selected = this.selected(); // change filter type
}

/*
 * In p5.js, draw() function is executed after setup(). This function runs inside a loop until the program is stopped.
*/
function draw()
{
    image(videoInput, 0, 0, outputWidth, outputHeight); // render video from webcam

    // apply filter based on choice
    switch(selected)
    {
        case '-1': break;
        case '0': drawSpidermanMask(); break;
    }
}

// Spiderman Mask Filter
function drawSpidermanMask()
{
    const positions = faceTracker.getCurrentPosition();
    if (positions !== false)
    {
        push();
        translate(-30, -10)
        const width = Math.abs(positions[15][0] - positions[19][0]) * 1.5;
        const height = Math.abs(positions[41][1] - Math.ceil((positions[16][1] + positions[20][1])/2)) * 1.5;
        image(imgSpidermanMask, positions[19][0], positions[19][1], width, height);
        pop();
    }
}

function drawDogFace()
{
    const positions = faceTracker.getCurrentPosition();
    if (positions !== false)
    {
        if (positions.length >= 20) {
            push();
            translate(-100, -150); // offset adjustment
            image(imgDogEarRight, positions[20][0], positions[20][1]);
            pop();
        }

        if (positions.length >= 16) {
            push();
            translate(-20, -150); // offset adjustment
            image(imgDogEarLeft, positions[16][0], positions[16][1]);
            pop();
        }

        if (positions.length >= 62) {
            push();
            translate(-57, -20); // offset adjustment
            image(imgDogNose, positions[62][0], positions[62][1]);
            pop();
        }
    }
}

function windowResized()
{
    const maxWidth = Math.min(windowWidth, windowHeight);
    pixelDensity(1);
    outputWidth = maxWidth;
    outputHeight = maxWidth * 2; // 4:3
    resizeCanvas(outputWidth, outputHeight);
}