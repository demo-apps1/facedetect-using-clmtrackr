
const htmlElements = {
    camVideo: $('#inputVideo')[0],
    outputCanvas: $('#canvas')[0],
    positionDescription: $('#positions'),
};

const faceData = {
    source: [],
    faceAvailable: false,
    availableCallback: 'faceDetected',
    notAvailableCallback: 'faceRemoved',
};

const cameraObject = new Camera(htmlElements.camVideo);
const clmTracker = new clm.tracker();

$(document).ready(() => {
    if (cameraObject.isReady()) {
        initCLMTrack();
    }
});

const initCLMTrack = () => {
    const input = cameraObject.videoSection();

    clmTracker.init();
    clmTracker.start(input);

    positionLoop();

    const canvasContext = htmlElements.outputCanvas.getContext('2d');
    function drawLoop() {
        requestAnimFrame(drawLoop);
        canvasContext.clearRect(0, 0, htmlElements.outputCanvas.width, htmlElements.outputCanvas.height);
        clmTracker.draw(htmlElements.outputCanvas);
    }
    drawLoop();
};

const positionLoop = async () => {
    await requestAnimFrame(positionLoop);
    const positions = clmTracker.getCurrentPosition();
    let positionString = ``;
    if (positions) {
        faceData.source = positions;
        if (!faceData.faceAvailable) {
            faceData.faceAvailable = true;
            callFunction(faceData.availableCallback);
        }

        let count = 10;
        positions.map((pos, index) => {
            if (count > 0) {
                positionString += 'Feature ' + index +' : '+ pos[0].toFixed(0) + ' ' + pos[1].toFixed(0);
                positionString += '<br/>';
                count --;
            }
        });
        htmlElements.positionDescription.html(positionString);
    } else {
        faceData.source = [];
        if (faceData.faceAvailable) {
            faceData.faceAvailable = false;
            callFunction(faceData.notAvailableCallback);
        }
    }
};

// helper functions

/**
 * Provides requestAnimationFrame in a cross browser way.
 */
const requestAnimFrame = ((callback) => {
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        (() => {
            return window.setTimeout(callback, 1000/60);
        })
})();

/**
 * Provides cancelRequestAnimationFrame in a cross browser way.
 */
const cancelRequestAnimFrame = (() => {
    return window.cancelAnimationFrame ||
        window.webkitCancelRequestAnimationFrame ||
        window.mozCancelRequestAnimationFrame ||
        window.oCancelRequestAnimationFrame ||
        window.msCancelRequestAnimationFrame ||
        window.clearTimeout;
})();

// video support utility functions
const supports_video = () => {
    return !!document.createElement('video').canPlayType;
};

const supports_h264_baseline_video = () => {
    if (!supports_video()) { return false; }
    const video = document.createElement("video");
    return video.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"');
};

const supports_webm_video = () => {
    if (!supports_video()) {
        return false;
    }
    const video = document.createElement("video");
    return video.canPlayType('video/webm; codecs="vp8"');
};

const faceDetected = function () {
    console.warn('Face detected');
};

const faceRemoved = function () {
    console.error('Face escaped');
};

const callFunction = (name) => {
    eval(name+ '()');
};