
class Camera {
    ready = true;
    streamingData = {
        stream: null,
    };
    elements = {
        showDiv: null
    };

    constructor(element) {
        this.elements.showDiv = element;
        this.constraints = {
            audio: false,
            video: {
                width: 640,
                height: 360
            }
        };

        try {
            navigator.mediaDevices.getUserMedia(this.constraints)
                .then((stream) => this.handleCameraInit(stream))
                .catch((exception) => this.callhandleNoCameraAccess(exception));
        } catch (exception) {
            this.handleNoCameraAccess(exception);
        }
    }

    handleCameraInit(stream) {
        this.streamingData.stream = stream;
        this.elements.showDiv.srcObject = stream;
        this.ready = true;
    }

    handleNoCameraAccess(exception = null) {
        console.log('Error', exception);
    }

    /**
     * @return {boolean}
     */
    isReady() {
        return this.ready;
    }

    videoSection() {
        return this.elements.showDiv;
    }
}